import crudTest from '../crudTestCase';
import api from './users.api';

const seed = {
  id: 1,
  first_name: 'John',
  last_name: 'Doe',
  date_of_birth: '1986-06-23',
};

const dataToCreate = {
  first_name: 'John',
  last_name: 'Two',
  date_of_birth: '1983-04-02',
};

const dataToUpdate = {
  date_of_birth: '1986-04-02'
};

const postValidationCase = [
  [
    {},
    {
      first_name: ['User.first_name cannot be null'],
      last_name: ['User.last_name cannot be null'],
      date_of_birth: ['User.date_of_birth cannot be null']
    },
  ], [
    {
      first_name: "John",
      last_name: null,
      date_of_birth: "coelho",
    },
    {
      last_name: ['User.last_name cannot be null'],
      date_of_birth: ['Is not a valid date']
    },
  ]
];

const putValidationCase = [
  [
    {
      first_name: "John",
      last_name: null,
      date_of_birth: null,
    },
    {
      last_name: ['User.last_name cannot be null'],
      date_of_birth: ['User.date_of_birth cannot be null']
    },
  ], [
    {
      last_name: null,
      date_of_birth: "coelho",
    },
    {
      last_name: ['User.last_name cannot be null'],
      date_of_birth: ['Is not a valid date']
    },
  ]
];

crudTest({
  api,
  model: 'User',
  seed,
  dataToCreate,
  dataToUpdate,
  postValidationCase,
  putValidationCase,
});
