import crudApi from '../crudApi';
import User from './users.model';

export default crudApi(User);
