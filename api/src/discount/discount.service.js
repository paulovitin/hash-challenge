import grpc from 'grpc';

import logger from '../logger';
import messages from './discount_pb';
import services from './discount_grpc_pb';

export default class DiscountService {

  static async get({ productId, userId }) {
    const client = new services.DiscountClient(
      process.env.GRPC, grpc.credentials.createInsecure(),
    );
    var request = new messages.Request();

    request.setUserId(userId);
    request.setProductId(String(productId));

    return new Promise((resolve, reject) => {
      client.get(request, (error, response) => {
        if (error) {
          return reject(error);
        }

        const pct = parseFloat(response.getPct().toFixed(2))
        const value_in_cents = response.getValueInCents();

        resolve({
          pct,
          value_in_cents,
        });
      });
    });
  }

  static async do(productRow, userId) {
    let product = {
      ...productRow,
    };

    try {
      const { pct, value_in_cents } = await DiscountService.get({
        productId: product.id,
        userId,
      });

      if (pct) {
        product = {
          ...product,
          discount: {
            value_in_cents,
            pct,
          }
        }
      }
    } catch (error) {
      logger.error(error);
    }

    return product;
  }
}
