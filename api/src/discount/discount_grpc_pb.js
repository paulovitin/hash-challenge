/* eslint-disable */
// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var discount_pb = require('./discount_pb.js');

function serialize_protobuffer_Request(arg) {
  if (!(arg instanceof discount_pb.Request)) {
    throw new Error('Expected argument of type protobuffer.Request');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_protobuffer_Request(buffer_arg) {
  return discount_pb.Request.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_protobuffer_Response(arg) {
  if (!(arg instanceof discount_pb.Response)) {
    throw new Error('Expected argument of type protobuffer.Response');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_protobuffer_Response(buffer_arg) {
  return discount_pb.Response.deserializeBinary(new Uint8Array(buffer_arg));
}


var DiscountService = exports.DiscountService = {
  get: {
    path: '/protobuffer.Discount/Get',
    requestStream: false,
    responseStream: false,
    requestType: discount_pb.Request,
    responseType: discount_pb.Response,
    requestSerialize: serialize_protobuffer_Request,
    requestDeserialize: deserialize_protobuffer_Request,
    responseSerialize: serialize_protobuffer_Response,
    responseDeserialize: deserialize_protobuffer_Response,
  },
};

exports.DiscountClient = grpc.makeGenericClientConstructor(DiscountService);
