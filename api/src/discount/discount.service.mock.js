import sinon from 'sinon';

import messages from './discount_pb';
import services from './discount_grpc_pb';

sinon.replace(messages, 'Request', function () {
  return {
    setUserId(val) {
      this.userId = String(val);
    },
    getUserId() {
      return this.userId;
    },
    setProductId(val) {
      this.productId = String(val);
    },
    getProductId() {
      return this.productId;
    },
  };
});

sinon.replace(services, 'DiscountClient', function() {
  return {
    get(req, cb) {
      if (req.getUserId() === 'fail') {
        return cb(new Error('Fail'), null);
      }

      let pctValue = 0.1000;
      let valueInCents = 70000;

      if (req.getUserId() !== '1') {
        pctValue = 0.0;
        valueInCents = 80000;
      }

      const response = {
        getPct() {
          return pctValue;
        },
        getValueInCents() {
          return valueInCents;
        }
      };

      return cb(null, response)
    }
  }
});
