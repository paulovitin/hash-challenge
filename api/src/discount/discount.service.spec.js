import DiscountService from './discount.service';
import './discount.service.mock';

const productId = 1;
const userId = 1;

describe.only('DiscountService', () => {
  it('should get method fail gracefully', async () => {
    expect.assertions(1);

    try {
      await DiscountService.get({ productId, userId: 'fail' });
    } catch (error) {
      expect(error.message).toBe('Fail');
    }
  });

  it('should get method return correct values', async () => {
    expect.assertions(2);
    const response = await DiscountService.get({ productId, userId });
    expect(response.pct).toBe(0.1);
    expect(response.value_in_cents).toBe(70000);
  });

  it('should do discount', async () => {
    expect.assertions(1);
    const product = {
      title: 'Product',
      description: 'Test',
      price_in_cents: 100000,
      id: productId,
    };

    const response = await DiscountService.do(product, userId);

    expect(response).toMatchObject({
      ...product,
      discount: {
        pct: 0.1,
        value_in_cents: 70000,
      },
    });
  });

  it('should do discount fail grpc', async () => {
    expect.assertions(2);
    global.console._stdout.write = jest.fn();

    const product = {
      title: 'Product',
      description: 'Test',
      price_in_cents: 100000,
      id: productId,
    };

    const response = await DiscountService.do(product, 'fail');

    expect(response).toMatchObject(product);
    expect(global.console._stdout.write).toHaveBeenCalled();
  });
});
