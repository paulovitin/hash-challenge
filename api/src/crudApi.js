import { Router } from 'express';

import { responseError } from './utils';

export default function crudApi(Model, override = null) {
  const routerApi = Router();

  if (override) {
    routerApi.use(override);
  }

  routerApi
    .get('/', async (req, res) => res.send(await Model.findAll()))

    .get('/:id', async ({ params: { id } }, res) => {
      const resource = await Model.findByPk(id);
      resource ? res.send(resource) : res.status(404).send();
    })

    .post('/', async ({ body }, res) => {
      try {
        const resource = await Model.create(body);
        return res.send(resource);
      } catch(error) {
        return responseError(res, error);
      }
    })

    .put('/:id', async ({ body, params: { id } }, res) => {
      const resource = await Model.findByPk(id);

      if (!resource) {
        res.status(404).send();
      }

      try {
        await resource.set(body).save();
        return res.send(resource);
      } catch(error) {
        return responseError(res, error);
      }
    })

    .delete('/:id', async ({ params: { id } }, res) => {
      const resource = await Model.findByPk(id);

      if (resource) {
        await resource.destroy();
      }

      res.status(204).send()
    })
  ;

  return routerApi;
}
