import crudTest from '../crudTestCase';
import api from './products.api';
import '../discount/discount.service.mock';

const seed = {
  id: 1,
  title: 'iPhone',
  description: 'The expensive smartphone',
  price_in_cents: 800000,
};

const dataToCreate = {
  title: 'iPhone',
  description: 'Two',
  price_in_cents: 800000,
};

const dataToUpdate = {
  price_in_cents: 1000000,
};

const productWithDiscount = {
  ...seed,
  discount: {
    pct: 0.1,
    value_in_cents: 70000,
  },
};

const postValidationCase = [
  [
    {},
    {
      description: ['Products.description cannot be null'],
      price_in_cents: ['Products.price_in_cents cannot be null'],
      title: ['Products.title cannot be null'],
    },
  ], [
    {
      title: "iPhone",
      description: null,
      price_in_cents: "coelho",
    },
    {
      description: ['Products.description cannot be null'],
      price_in_cents: ['Validation isInt on price_in_cents failed']
    },
  ]
];

const putValidationCase = [
  [
    {
      title: "iPhone",
      description: null,
      price_in_cents: "a39",
    },
    {
      description: ['Products.description cannot be null'],
      price_in_cents: ['Validation isInt on price_in_cents failed']
    },
  ],
];

crudTest({
  api,
  model: 'Products',
  seed,
  dataToCreate,
  dataToUpdate,
  postValidationCase,
  putValidationCase,
  tests(requestApp) {

    it(`should return discounts for valid userId`, (done) => requestApp
      .get('/')
      .set('x-user-id', 1)
      .expect(200)
      .end(function (err, response) {
        expect(err).toBe(null);

        expect(response.body).toMatchObject([productWithDiscount]);

        done();
      })
    );

    it(`should return a product with discount for valid userId`, (done) => requestApp
      .get(`/${seed.id}`)
      .set('x-user-id', 1)
      .expect(200)
      .end(function (err, response) {
        expect(err).toBe(null);

        expect(response.body).toMatchObject(productWithDiscount);

        done();
      })
    );

    it(`should not return discounts for invalid userId`, (done) => requestApp
      .get(`/${seed.id}`)
      .set('x-user-id', 2)
      .expect(200)
      .end(function (err, response) {
        expect(err).toBe(null);
        expect(response.body).toMatchObject(seed);
        expect(response.body.discount).toBe(undefined);

        done();
      })
    );
  },
});
