import { Model } from 'sequelize';

import DiscountService from '../discount/discount.service';

export default class Products extends Model {
  static init(sequelize, DataTypes) {
    return super.init({
      price_in_cents: {
        allowNull: false,
        type: DataTypes.INTEGER,
        validate: {
          isInt: true,
        },
      },
      title: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      description: {
        allowNull: false,
        type: DataTypes.TEXT
      },
    }, {
      sequelize,
      tableName: 'products',
    });
  }

  static async findAll({ withUserId, ...options }) {
    const productsList = await super.findAll(options);

    if (!withUserId) {
      return productsList;
    }

    const products = await Promise.all(
      productsList.map((product) => DiscountService.do(product.get(), withUserId)),
    );

    return products;
  }

  static async get(id, { withUserId, ...options }) {
    const product = await super.findByPk(id, options);

    if (!withUserId) {
      return product;
    }

    return  DiscountService.do(product.get(), withUserId);
  }
}
