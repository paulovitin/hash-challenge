import { Router } from 'express';

import crudApi from '../crudApi';
import Product from './products.model'

export default crudApi(
  Product,
  Router()
    .get('/', async ({ headers: { 'x-user-id': withUserId } }, res) =>
      res.send(await Product.findAll({ withUserId }))
    )
    .get('/:id', async (req, res) => {
      const { 'x-user-id': withUserId } = req.headers;
      const { id } = req.params;
      const product = await Product.get(id, { withUserId });
      product ? res.send(product) : res.status(404).send();
    })
);
