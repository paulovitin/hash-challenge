import Sequelize from 'sequelize';
import dotenv from 'dotenv';

import Product from './products/products.model';
import User from './users/users.model';

dotenv.config();

const db = {};
const env = process.env;
const DB_URL = env.NODE_ENV !== 'test' ? env.DB_URL : env.DB_TEST_URL;
const sequelize = new Sequelize(DB_URL, { logging: false });

const product = Product.init(sequelize, Sequelize);
db[product.name] = product;
const user = User.init(sequelize, Sequelize);
db[user.name] = user;

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
