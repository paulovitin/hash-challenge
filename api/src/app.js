import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import express, { Router } from 'express';

import database from './database';
import logger from './logger';
import productsApi from './products/products.api';
import usersApi from './users/users.api';

dotenv.config();

const app = express()
  .use(bodyParser.json())
  .set('json spaces', 2);

const router = Router();

database.sequelize.sync();

router.use('/products', productsApi);
router.use('/users', usersApi);

app.use(logger.express)
  .use(router);

const server = app.listen(process.env.PORT || 3000, () => (
  logger.info(`Hash API running in port ${server.address().port}`)
));
