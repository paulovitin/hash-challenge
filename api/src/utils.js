import { ValidationError } from 'sequelize';

export const validationError = (errors = []) => errors
  .reduce((acc, { message, path }) => ({
    ...acc,
    [path]: [
      ...(acc[path] || []),
      message,
    ],
  }), {});

export const responseError = (res, error) => {
  if (error instanceof ValidationError) {
    const { errors } = error;
    return res.status(400).send(validationError(errors));
  }

  return res.status(500).send();
};
