import bodyParser from 'body-parser';
import express from 'express';
import request from 'supertest';

import db from './database';

export default function crudTest({
  api, model, seed, dataToCreate, dataToUpdate, postValidationCase,
  putValidationCase, tests,
}) {
  const app = express()
    .use(bodyParser.json())
    .use(api);

  const objectUpdate = {
    ...seed,
    ...dataToUpdate,
  }

  describe(`${model} API`, () => {
    beforeAll(async () => {
      await db[model].sync();
    });

    beforeEach(async () => {
      await db[model].create(seed);
    });

    afterEach(async () => {
      await db[model].destroy({
        where: {},
        truncate: true
      });
    });

    it(`should get all ${model}s`, (done) => request(app)
      .get('/')
      .expect(200)
      .end((err, response) => {
        expect(err).toBe(null);
        expect(response.body[0]).toMatchObject(seed);
        done();
      })
    );

    it(`should get ${model} with id ${seed.id}`, (done) => request(app)
      .get(`/${seed.id}`)
      .expect(200)
      .end((err, response) => {
        expect(err).toBe(null);
        expect(response.body).toMatchObject(seed);
        done();
      })
    );

    it(`should create ${model}`, (done) => request(app)
      .post('/')
      .send(dataToCreate)
      .expect(200)
      .end(function(err, response) {
        expect(err).toBe(null);
        expect(response.body).toMatchObject(dataToCreate);
        done();
      })
    );

    it(`should update user with id ${seed.id}`, (done) => request(app)
      .put(`/${seed.id}`)
      .send(objectUpdate)
      .expect(200)
      .end(function(err, response) {
        expect(err).toBe(null);
        expect(response.body).toMatchObject(objectUpdate);
        done();
      })
    );

    it(`should delete ${model} with id ${seed.id}`, (done) => request(app)
      .delete(`/${seed.id}`)
      .expect(204, done)
    );

    test.each(postValidationCase)(
      `should try create an invalid ${model} receive 400`,
      (send, error, done) => request(app)
        .post('/')
        .send(send)
        .expect(400)
        .end(function(err, response) {
          expect(response.body).toMatchObject(error);
          done();
        })
    );

    test.each(putValidationCase)(
      `should try update ${model} with invalid data receive 400`,
      (send, error, done) => request(app)
        .put(`/${seed.id}`)
        .send(send)
        .expect(400)
        .end(function(err, response) {
          expect(response.body).toMatchObject(error);
          done();
        })
      );

    if (tests) {
      tests(request(app));
    }
  });
}
