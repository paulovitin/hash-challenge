import winston from 'winston';
import expressWinston from 'express-winston';

const errorStackFormat = winston.format(info => {
  if (info instanceof Error) {
    return Object.assign({}, info, {
      stack: info.stack,
      message: info.message
    })
  }
  return info
})

const loggerOptions = {
  transports: [
    new winston.transports.Console(),
  ],
  format: winston.format.combine(
    winston.format.json(),
    errorStackFormat(),
  ),
  meta: true,
  msg: "HTTP {{req.method}} {{req.url}} {{res.statusCode}}",
  expressFormat: true,
  colorize: false,
};
const logger = winston.createLogger(loggerOptions);
logger.express = expressWinston.logger(loggerOptions);

export default logger;
