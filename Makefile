api-test:
	docker-compose run --rm api yarn test

discount-test:
	docker-compose run --rm discount go test -v ./...
