
# Hash Backend Challenge

> **Mantenedor:**  [Paulo Reis](https://github.com/paulovitin)

## Índice
* [Estrutura](#estrutura)
	* [Database](#database)
	* [Hash API (NodeJS)](#hash-api)
	* [Discount GRPC (Go)](#discount-grpc)
* [Primeiros passos](#primeiros-passos)
	* [Executando o Projeto](#executando-o-projeto)
	* [Testes](#testes)
* [Customização](#customização)

## Estrutura
A estrutura básica do projeto se dá na seguinte forma

### Database
Está sendo utilizando um banco Postgres rodando na porta `5432` na versão `10.7`.

### Hash API
Restful API escrita em NodeJS que rodará na porta `8080`.

#### Users
`GET [/users]` - Retorna uma lista de usuários cadastros.

`GET [/users/:id]` - Retorna um usuário caso exista.

`POST [/users]` - Cria um novo usuario.
```json
{
	"first_name": "John",
	"last_name": "Doe",
	"date_of_birth": "1986-06-23"
}
```
`PUT [/users/:id]` - Atualiza dados do usuário.

`DELETE [/users:id]`- Apaga registro do usuário.

####  Products
`GET [/products]` - Retorna uma lista de produtos cadastros. É possível passar X-USER-ID nos headers para identificar um usuário registrado.

`GET [/products/:id]` - Retorna um produto caso exista. Também pode receber X-USER-ID para identificar um usuário.

`POST [/products]` - Cria um novo produto.
```json
{
	"title": "iPhone",
	"description": "Very expensive smartphone",
	"price_in_cents": 1000000
}
```

`PUT [/products/:id]` - Atualiza dados do produto.

`DELETE [/products:id]`- Apaga registro do produto.

### Discount GRPC
Serviço escrito em Golang na porta `50051`.

## Primeiros passos

### Executando o Projeto
Para começar execute no terminal:
```bash
$ docker-compose up
```

### Testes
Para rodar os testes basta executar o comando para cada serviço:

Para testes da API Restful
```bash
$ make api-test
```

Para testes do Discount service
```bash
$ make discount-test
```

## Customização

O serviços podem ser customizados através das seguintes envs localizados no docker-compose.yml

| ENV | PADRãO | DESCRIÇÃO |
|--|--|--|
| BIRTHDAY_DISCOUNT | 0.05 (5%) | Desconto para aniversário |
| BLACKFRIDAY_DISCOUNT | 0.1 (10%) | Desconto para BlackFriday |
| MAX_DISCOUNT | 0.1 (10%) | Desconto máximo concedido por usuário |
| BLACKFRIDAY_DAY | 25 | Dia da BlackFriday |
| BLACKFRIDAY_MONTH | 11 | Mês da BlackFriday |
