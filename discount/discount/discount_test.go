package discount

import (
	"testing"
	"time"

	"github.com/paulovitin/discount/product"
	"github.com/paulovitin/discount/user"
	"github.com/stretchr/testify/assert"
)

func mockUser(dateOfBirth string) (user user.User) {
	user.DateOfBirth, _ = time.Parse("2006-01-02", dateOfBirth)
	return
}

func mockProduct(priceInCents int) (product product.Product) {
	product.PriceInCents = priceInCents
	return
}

func TestDiscountSever(t *testing.T) {
	birth := "1986-06-23"
	s := Server{
		BirthdayDiscount:    0.05,
		BlackFridayDiscount: 0.1,
		BlackFridayDay:      25,
		BlackFridayMonth:    11,
		MaxDiscount:         0.1,
	}

	t.Run("Test discount method by birthday", func(t *testing.T) {
		today, _ := time.Parse("2006-01-02", "2019-06-23")
		s.Today = today
		pct := s.Discount(mockUser(birth))
		assert.Equal(t, pct, s.BirthdayDiscount)
	})

	t.Run("Test discount method by blackfriday", func(t *testing.T) {
		today, _ := time.Parse("2006-01-02", "2019-11-25")
		s.Today = today
		pct := s.Discount(mockUser(birth))
		assert.Equal(t, pct, s.BlackFridayDiscount)
	})

	t.Run("Test discount with maxDiscount", func(t *testing.T) {
		today, _ := time.Parse("2006-01-02", "2019-11-25")
		birth = "1986-11-25"
		s.Today = today
		pct := s.Discount(mockUser(birth))
		assert.Equal(t, pct, s.MaxDiscount)
	})

	t.Run("Test increase maxDiscount black+birthday", func(t *testing.T) {
		today, _ := time.Parse("2006-01-02", "2019-11-25")
		birth = "1986-11-25"
		s.Today = today
		s.MaxDiscount = 1.5
		pct := s.Discount(mockUser(birth))
		assert.Equal(t, pct, s.BlackFridayDiscount+s.BirthdayDiscount)
	})
}
