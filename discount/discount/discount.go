package discount

import (
	"context"
	"log"
	"time"

	"github.com/paulovitin/discount/product"
	pb "github.com/paulovitin/discount/protobuffer"
	"github.com/paulovitin/discount/user"
)

// Server Discount GRPC
type Server struct {
	Product             product.Repository
	Today               time.Time
	User                user.Repository
	BirthdayDiscount    float32
	BlackFridayDiscount float32
	BlackFridayDay      int
	BlackFridayMonth    int
	MaxDiscount         float32
}

// Discount get the user discount
func (s *Server) Discount(user user.User) (discount float32) {
	today := s.Today
	if today.Day() == s.BlackFridayDay && s.BlackFridayMonth == int(today.Month()) {
		discount += s.BlackFridayDiscount
	}

	userBirth := user.DateOfBirth

	if today.Day() == userBirth.Day() && today.Month() == userBirth.Month() {
		discount += s.BirthdayDiscount
	}

	if discount > s.MaxDiscount {
		discount = s.MaxDiscount
	}

	return
}

// Get implements Server Discount
func (s *Server) Get(ctx context.Context, req *pb.Request) (*pb.Response, error) {
	user, err := s.User.Get(req.UserId)
	if err != nil {
		log.Println(err)
	}

	product, err := s.Product.Get(req.ProductId)
	if err != nil {
		log.Println(err)
	}

	discount := s.Discount(user)
	priceInCentsFloat := float32(product.PriceInCents)
	valueInCents := int32(priceInCentsFloat - (priceInCentsFloat * discount))

	return &pb.Response{Pct: discount, ValueInCents: valueInCents}, nil
}
