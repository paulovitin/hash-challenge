package test

import (
	"log"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
)

type Env struct {
	BirthdayDiscount    string `default:"0.05"`
	BlackfridayDiscount string `default:"0.1"`
	BlackfridayDay      string `default:"25"`
	BlackfridayMonth    string `default:"11"`
	MaxDiscount         string `default:"0.1"`
}

func SetupDB() (*sqlx.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Panicf("an error '%s' was not expected when opening a stub database connection", err)
	}
	sqlxDB := sqlx.NewDb(db, "sqlmock")

	return sqlxDB, mock
}
