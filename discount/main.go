package main

import (
	"log"
	"net"
	"os"
	"strconv"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/paulovitin/discount/discount"
	"github.com/paulovitin/discount/product"
	pb "github.com/paulovitin/discount/protobuffer"
	"github.com/paulovitin/discount/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var database *sqlx.DB

func getEnvInt(name string) (value int) {
	value, err := strconv.Atoi(os.Getenv(name))
	if err != nil {
		log.Panic(err)
	}
	return
}

func getEnvFloat32(name string) float32 {
	value, err := strconv.ParseFloat(os.Getenv(name), 64)
	if err != nil {
		log.Panic(err)
	}
	return float32(value)
}

func init() {
	connection, err := sqlx.Open("postgres", os.Getenv("DB_URL"))
	if err != nil {
		log.Panic(err)
	}

	database = connection
}

func main() {
	listener, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Panic(err)
	}

	product := product.Repository{DB: database}
	user := user.Repository{DB: database}

	s := grpc.NewServer()
	d := &discount.Server{
		Product:             product,
		User:                user,
		Today:               time.Now(),
		BirthdayDiscount:    getEnvFloat32("BIRTHDAY_DISCOUNT"),
		BlackFridayDiscount: getEnvFloat32("BLACKFRIDAY_DISCOUNT"),
		BlackFridayDay:      getEnvInt("BLACKFRIDAY_DAY"),
		BlackFridayMonth:    getEnvInt("BLACKFRIDAY_MONTH"),
		MaxDiscount:         getEnvFloat32("MAX_DISCOUNT"),
	}
	pb.RegisterDiscountServer(s, d)

	reflection.Register(s)
	if err := s.Serve(listener); err != nil {
		log.Panic(err)
	}
}
