module github.com/paulovitin/discount

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/golang/mock v1.2.0 // indirect
	github.com/golang/protobuf v1.3.1
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.0.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.3.0
	github.com/vektra/mockery v0.0.0-20181123154057-e78b021dcbb5 // indirect
	golang.org/x/net v0.0.0-20190328230028-74de082e2cca
	google.golang.org/grpc v1.19.1
)
