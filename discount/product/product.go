package product

import (
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

// Product struct
type Product struct {
	PriceInCents int `db:"price_in_cents"`
}

// Repository implements ProductRepository
type Repository struct {
	DB *sqlx.DB
}

// Get returns Product by id
func (p *Repository) Get(id string) (Product, error) {
	product := Product{}
	err := p.DB.Get(&product, "SELECT price_in_cents FROM products WHERE id = $1", id)
	if err != nil {
		err = errors.Wrapf(err, "Product.Repository.Get, Product not found")
	}
	return product, err
}
