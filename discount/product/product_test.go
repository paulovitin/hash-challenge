package product

import (
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/paulovitin/discount/test"
	"github.com/stretchr/testify/assert"
)

func TestProductRepository(t *testing.T) {
	db, mock := test.SetupDB()
	defer db.Close()

	repository := Repository{DB: db}
	queryRegex := "SELECT price_in_cents FROM products WHERE id = \\$1"
	productID := "1"

	t.Run("Test Get Product", func(t *testing.T) {
		priceInCents := 10000

		rows := sqlmock.NewRows([]string{"price_in_cents"}).
			AddRow(priceInCents)

		mock.ExpectQuery(queryRegex).
			WithArgs(productID).
			WillReturnRows(rows)

		user, err := repository.Get(productID)
		assert.Nil(t, err)
		assert.Equal(t, user.PriceInCents, priceInCents)
	})

	t.Run("Test Get Product Not Exists", func(t *testing.T) {
		mock.ExpectQuery(queryRegex).
			WithArgs(productID).
			WillReturnError(fmt.Errorf("some error"))

		_, err := repository.Get(productID)
		assert.NotNil(t, err)
	})
}
