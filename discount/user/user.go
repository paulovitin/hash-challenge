package user

import (
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

// User struct
type User struct {
	DateOfBirth time.Time `db:"date_of_birth"`
}

// Repository implements UserRepository interface
type Repository struct {
	DB *sqlx.DB
}

// Get returns User by id
func (u *Repository) Get(id string) (User, error) {
	user := User{}
	err := u.DB.Get(&user, "SELECT date_of_birth FROM users WHERE id = $1", id)
	if err != nil {
		err = errors.Wrapf(err, "User.Repository.Get, User not found")
	}
	return user, err
}
