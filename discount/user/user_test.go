package user

import (
	"fmt"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/paulovitin/discount/test"
	"github.com/stretchr/testify/assert"
)

func TestUserRepository(t *testing.T) {
	db, mock := test.SetupDB()

	defer db.Close()

	repository := Repository{DB: db}
	queryRegex := "SELECT date_of_birth FROM users WHERE id = \\$1"
	userID := "1"

	t.Run("Test Get User", func(t *testing.T) {
		today, _ := time.Parse("2006-01-02", "1986-06-23")

		rows := sqlmock.NewRows([]string{"date_of_birth"}).
			AddRow(today)

		mock.ExpectQuery(queryRegex).
			WithArgs(userID).
			WillReturnRows(rows)

		user, err := repository.Get(userID)
		assert.Nil(t, err)
		assert.Equal(t, user.DateOfBirth, today)
	})

	t.Run("Test Get User Not Exists", func(t *testing.T) {
		mock.ExpectQuery(queryRegex).
			WithArgs(userID).
			WillReturnError(fmt.Errorf("some error"))

		_, err := repository.Get(userID)
		assert.NotNil(t, err)
	})
}
